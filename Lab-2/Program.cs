﻿using System;

namespace Lab_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the initial value of x1min: ");
            string sx1min = Console.ReadLine();
            double x1min = double.Parse(sx1min);

            Console.WriteLine("Enter the final value of x1max: ");
            string sx1max = Console.ReadLine();
            double x1max = double.Parse(sx1max);

            Console.WriteLine("Enter value of dx1: ");
            string sdx1 = Console.ReadLine();
            double dx1 = double.Parse(sdx1);

            Console.WriteLine("Enter the initial value of x2min: ");
            string sx2min = Console.ReadLine();
            double x2min = double.Parse(sx2min);

            Console.WriteLine("Enter the final value of x2max: ");
            string sx2max = Console.ReadLine();
            double x2max = double.Parse(sx2max);

            Console.WriteLine("Enter value of dx2: ");
            string sdx2 = Console.ReadLine();
            double dx2 = double.Parse(sdx2);

            double x1 = x1min, cos_prod = 1;
            double x2, y, math_cos;
            for (; x1 <= x1max; x1 += dx1)
            {
                x2 = x2min;
                for (; x2 <= x2max; x2 += dx2)
                {
                    math_cos = Math.Cos(2 * x2);
                    y = Math.Sqrt(Math.Abs(math_cos) + (x1 / x2)) / Math.Sqrt(16 * x1 * x2);
                    Console.WriteLine("x1 = {0:0.0000}\t\t x2 = {1:0.0000} \t\t y = {2:0.0000}", x1, x2, y);

                    //Console.WriteLine($"{math_cos}");

                    if (math_cos < 0 && !Double.IsNaN(math_cos)) cos_prod *= math_cos;
                }
            }

            Console.WriteLine($"The product of negative Cos = {cos_prod}");
            //Console.WriteLine("Press any key to close this window...");
            //Console.ReadKey();
        }
    }
}
