﻿using System;
using Lab08Lib;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Lab_8
{
    public partial class fRegion : Form
    {
        public _Region TheRegion;
        public fRegion(_Region r)
        {
            TheRegion = r;

            InitializeComponent();
        }

        

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void FRegion_Load(object sender, EventArgs e)
        {
            if(TheRegion != null)
            {
                tbName.Text = TheRegion.Name;
                tbCountry.Text = TheRegion.Country;
                tbNationalLanguage.Text = TheRegion.NationalLanguage;
                tbInternetDomain.Text = TheRegion.InternetDomain;
                tbPopulation.Text = TheRegion.Population.ToString();
                tbAnnualIncome.Text = TheRegion.AnnualIncome.ToString("0.00");
                tbSquare.Text = TheRegion.Square.ToString("0.000");
                chbHasPort.Checked = TheRegion.HasAirport;
                chbHasAirport.Checked = TheRegion.HasAirport;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            TheRegion.Name = tbName.Text.Trim();
            TheRegion.Country = tbCountry.Text.Trim();
            TheRegion.NationalLanguage = tbNationalLanguage.Text.Trim();
            TheRegion.InternetDomain = tbInternetDomain.Text.Trim();
            TheRegion.Population = int.Parse(tbPopulation.Text.Trim());
            TheRegion.AnnualIncome = double.Parse(tbAnnualIncome.Text.Trim());
            TheRegion.Square = double.Parse(tbSquare.Text.Trim());
            TheRegion.HasPort = chbHasPort.Checked;
            TheRegion.HasAirport = chbHasAirport.Checked;

            DialogResult = DialogResult.OK;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
