﻿namespace Lab_8
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbRegionsInfo = new System.Windows.Forms.TextBox();
            this.btnAddRegion = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbRegionsInfo
            // 
            this.tbRegionsInfo.Location = new System.Drawing.Point(12, 12);
            this.tbRegionsInfo.Multiline = true;
            this.tbRegionsInfo.Name = "tbRegionsInfo";
            this.tbRegionsInfo.ReadOnly = true;
            this.tbRegionsInfo.Size = new System.Drawing.Size(602, 272);
            this.tbRegionsInfo.TabIndex = 0;
            // 
            // btnAddRegion
            // 
            this.btnAddRegion.Location = new System.Drawing.Point(638, 33);
            this.btnAddRegion.Name = "btnAddRegion";
            this.btnAddRegion.Size = new System.Drawing.Size(108, 22);
            this.btnAddRegion.TabIndex = 1;
            this.btnAddRegion.Text = "Додати місто";
            this.btnAddRegion.UseVisualStyleBackColor = true;
            this.btnAddRegion.Click += new System.EventHandler(this.BtnAddRegion_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(638, 216);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(108, 22);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Закрити";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 309);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAddRegion);
            this.Controls.Add(this.tbRegionsInfo);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота №8";
            this.Load += new System.EventHandler(this.FMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbRegionsInfo;
        private System.Windows.Forms.Button btnAddRegion;
        private System.Windows.Forms.Button btnClose;
    }
}

