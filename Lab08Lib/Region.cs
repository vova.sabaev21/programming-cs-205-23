﻿using System;

namespace Lab08Lib
{
    public class _Region
    {
        public string Name;
        public string Country;
        public string NationalLanguage;
        public string InternetDomain;

        public int Population;
        public double AnnualIncome;

        public double Square;

        public bool HasPort;
        public bool HasAirport;

        public _Region()
        {

        }

        public double AverangeIncome()
        {
            return this.AnnualIncome / this.Population;
        }
    }
}
