﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_9
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        static double Function(double x1, double x2)
        {
            return 0.25 * Math.Sqrt((x2 * x2 + (x1 / x2)) / (x1 * x2));
        }

        private void BtnCalc_Click(object sender, EventArgs e)
        {
            gv.GridColor = Color.Black;

            double x1min = double.Parse(tbx1min.Text);
            double x1max = double.Parse(tbx1max.Text);
            double x2min = double.Parse(tbx2min.Text);
            double x2max = double.Parse(tbx2max.Text);
            double dx1 = double.Parse(tbdx1.Text);
            double dx2 = double.Parse(tbdx2.Text);

            gv.ColumnCount = (int)Math.Truncate((x2max - x2min) / dx2) + 1;
            gv.RowCount = (int)Math.Truncate((x1max - x1min) / dx1) + 1;

            for (int i = 0; i < gv.RowCount; i++)
            {
                gv.Rows[i].HeaderCell.Value = (x1min + i * dx1).ToString("0.000");
                gv.RowHeadersWidth = 80;
            }
            for (int i = 0; i < gv.ColumnCount; i++)
            {
                gv.Columns[i].HeaderCell.Value = (x2min + i * dx2).ToString("0.000");
                gv.Columns[i].Width = 60;
            }

            int cl, rw;
            double x1, x2, y, sum = 0;

            rw = 0;
            x1 = x1min;

            while (x1 <= x1max)
            {
                x2 = x2min;
                cl = 0;
                while (x2 <= x2max)
                {
                    y = Function(x1, x2);
                    if(Math.Cos(y) > 0)
                    {
                        sum += y;
                    }
                    gv.Rows[rw].Cells[cl].Value = y.ToString("0.000");
                    x2 += dx2; cl++;
                }
                x1 += dx1;
                rw++;
            }

            double rowssum = 0;

            DataGridViewTextBoxColumn rsum;
            rsum = new DataGridViewTextBoxColumn();
            rsum.HeaderText = "Rows Sum";
            gv.Columns.Add(rsum);
            int col = rsum.Index;


            for (int i = 0; i < gv.RowCount; i++)
            {
                for (int j = 0; j < gv.ColumnCount; j++)
                {
                    rowssum += Double.Parse(gv.Rows[i].Cells[j].Value.ToString());
                    gv.Rows[i].Cells[col].Value = rowssum;
                }
            }
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            tbx1min.Text = "";
            tbx1max.Text = "";
            tbx2min.Text = "";
            tbx2max.Text = "";
            tbdx1.Text = "";
            tbdx2.Text = "";
            gv.Rows.Clear();
            for (int Cl = 0; Cl < gv.ColumnCount; Cl++) gv.Columns[Cl].HeaderCell.Value = "";
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити програму?", "Вихід з програми", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }

        private void Gv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
