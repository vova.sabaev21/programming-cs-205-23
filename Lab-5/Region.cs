﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_5
{
    class Region
    {
        public string _name;
        public string _country;
        public string _timezone;
        public string _nLanguage;
        public string _internetDomain;

        public int _population;
        public int _annualincome;
        public int _rAge;

        public double _square;

        public Region()
        {

        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public string TimeZone
        {
            get { return _timezone; }
            set { _timezone = value; }
        }

        public string NationalLanguage
        {
            get { return _nLanguage; }
            set { _nLanguage = value; }
        }

        public string InternetDomain
        {
            get { return _internetDomain; }
            set { _internetDomain = value; }
        }

        public int Population
        {
            get { return _population; }
            set { _population = value; }
        }

        public int rAge
        {
            get { return _rAge; }
            set { _rAge = value; }
        }

        public double Square
        {
            get { return _square; }
            set { _square = value; }
        }

        public int AnnualIncome
        {
            get { return _annualincome; }
            set { _annualincome = value; }
        }

        public int RegionAge()
        {
            int year = DateTime.Now.Year;
            year = year - this.rAge;
            return (year < 0) ? 0 : year;
        }

        public double AverangeIncome()
        {
            return this.AnnualIncome / this.Population;
        }

    }
}
